//
//  ListTableViewCell.swift
//  NewsReader
//
//  Created by Renat Gafarov on 04/05/2017.
//  Copyright © 2017 Renat Gafarov. All rights reserved.
//

import UIKit

class ListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel:UILabel!
    @IBOutlet weak var detailLabel:UILabel!
    
    static let identifier = "feedCell"
    
    func configure(with viewModel:FeedRepesentable){
        titleLabel.text = viewModel.title
        detailLabel.text = viewModel.date
    }

}
