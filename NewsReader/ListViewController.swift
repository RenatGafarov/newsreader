//
//  ListViewController.swift
//  NewsReader
//
//  Created by Renat Gafarov on 04/05/2017.
//  Copyright © 2017 Renat Gafarov. All rights reserved.
//

import UIKit

class ListViewController: UIViewController, ListViewModelDelegate {

    var viewModel:ListViewModelProtocol!
    @IBOutlet weak var feedsTableView:UITableView!
    private var refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupPullToRefresh()
        viewModel.update()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setupPullToRefresh(){
        if #available(iOS 10.0, *) {
            feedsTableView.refreshControl = refreshControl
        } else {
            feedsTableView.addSubview(refreshControl)
        }
        refreshControl.addTarget(viewModel, action: #selector(ListViewModel.refresh), for: .valueChanged)
    }
    
    func startRefresh() {
        refreshControl.beginRefreshing()
    }
    
    
    
    func endRefresh() {
        refreshControl.endRefreshing()
    }
    
    
    func didUpdatedViewModel() {
        feedsTableView?.reloadData()
    }
    

}

extension ListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfFeeds
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ListTableViewCell.identifier, for: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? ListTableViewCell, let cellViewModel = viewModel.viewModelForFeedWith(index: indexPath.row) else {
            return
        }
        
        cell.configure(with: cellViewModel)
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let feed = viewModel.viewModelForFeedWith(index: indexPath.row),
            let previewViewController = storyboard?.instantiateViewController(withIdentifier: NSStringFromClass(PreviewNewsViewController.self)) else {
                return
        }
        
        if let previewViewController = previewViewController as? PreviewNewsViewController {
            let preViewViewModel = PreviewViewModel(with: feed.url)
            previewViewController.viewModel = preViewViewModel
            navigationController?.pushViewController(previewViewController, animated: true)
        }
        
    }
    
    @IBAction func aboutMeButtonTapped(_ sender: UIBarButtonItem) {
        
        performSegue(withIdentifier: "aboutMeSegue", sender: nil)
        
    }
    
}
