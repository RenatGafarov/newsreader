//
//  AboutMeViewController.swift
//  NewsReader
//
//  Created by Renat Gafarov on 04/05/2017.
//  Copyright © 2017 Renat Gafarov. All rights reserved.
//

import UIKit

class AboutMeViewController: UIViewController {
    
    @IBOutlet weak var nameLabel:UILabel!
    @IBOutlet weak var phoneLabel:UILabel!
    @IBOutlet weak var emailLabel:UILabel!
    @IBOutlet weak var telegramLabel:UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        createViews(name: "Renat Gafarov", phone: "+7(999)450-451-0", email: "renatgafarov@icloud.com", telegram: "@reClosure")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startAnimations()

    }
    
    func startAnimations() {

        nameAnimation()
        phoneAnimation()
        emailAnimation()
        telegramAnimation()

    }
    
    func createViews(name:String,phone:String,email:String,telegram:String) {
        
        nameLabel.text = name
        phoneLabel.text = phone
        emailLabel.text = email
        telegramLabel.text = telegram
        
        nameLabel.sizeToFit()
        phoneLabel.sizeToFit()
        emailLabel.sizeToFit()
        telegramLabel.sizeToFit()

        nameLabel.frame.origin.x = view.frame.width
        nameLabel.frame.origin.y = 200
        
        phoneLabel.frame.origin.x = view.frame.width
        phoneLabel.frame.origin.y = nameLabel.frame.origin.y + 150
        
        emailLabel.frame.origin.x = view.frame.width
        emailLabel.frame.origin.y = phoneLabel.frame.origin.y + 50
        
        telegramLabel.frame.origin.x = view.frame.width
        telegramLabel.frame.origin.y = emailLabel.frame.origin.y + 50
        
    }
    
    func nameAnimation() {
        
        UIView.animate(withDuration: 0.4, delay: 0, options: [], animations: {
            self.nameLabel.center.x = self.view.center.x
        })
        
    }
    
    func phoneAnimation() {
        
        UIView.animate(withDuration: 0.5, delay: 0.2, options: [], animations: {
            self.phoneLabel.center.x = self.view.center.x
        })
        
    }
    
    func emailAnimation() {

        
        UIView.animate(withDuration: 0.5, delay: 0.3, options: [], animations: {
            self.emailLabel.center.x = self.view.center.x
        })
        
    }
    
    func telegramAnimation() {

        UIView.animate(withDuration: 0.5, delay: 0.4, options: [], animations: {
            self.telegramLabel.center.x = self.view.center.x
        })
        
    }

}
