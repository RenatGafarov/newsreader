//
//  PreviewViewModel.swift
//  NewsReader
//
//  Created by Renat Gafarov on 04/05/2017.
//  Copyright © 2017 Renat Gafarov. All rights reserved.
//

import Foundation

class PreviewViewModel {
    
    let url:URL
    
    init(with url:URL) {
        self.url = url
    }
    
    func buildRequest() -> URLRequest {
        let urlRequest = URLRequest(url: url)
        return urlRequest
    }
    
}
