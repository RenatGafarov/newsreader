//
//  FeedProvider.swift
//  NewsReader
//
//  Created by Renat Gafarov on 03/05/2017.
//  Copyright © 2017 Renat Gafarov. All rights reserved.
//

import Foundation
import CoreData

class FeedProvider: Storable {
    
    private let newsAPI:APIProtocol
    weak var accessor: StorageAccessor?
    
    
    
    lazy var objectConverter:ManagedObjectAndDTOConverter = {

        return ManagedObjectAndDTOConverter(with: self.context)

    }()
    
    
    
    init(api:APIProtocol) {

        self.newsAPI = api
        // 5 minutes
        Timer.scheduledTimer(timeInterval: 5 * 60.0, target: self, selector: #selector(FeedProvider.reloadData), userInfo: nil, repeats: true)

    }
    
    @objc func reloadData() {
        saveObjects()
    }
    
    
    func update(){

        if let feeds = requestFeeds() {

            let dto = obtainFeedDTObjects(feeds: feeds)
            accessor?.feeds = dto

        } else {
            reloadData()
        }

    }
    
    private func obtainFeedDTObjects(feeds:[Feed]) -> [FeedDTO]? {

        let result = objectConverter.convertToDTOs(from: feeds)
        return result

    }
    
    private func requestFeeds() -> [Feed]? {
        
        let request = Feed.feedRequest()
        let dateDescriptor = NSSortDescriptor(key: "date", ascending: false)
        request.sortDescriptors = [dateDescriptor]
        var result:[Feed]?

        do {
            result = try context.fetch(request)
        } catch let error {
            print(error.localizedDescription)
        }

        return result
    }

    private func saveObjects() {
        
        newsAPI.recieveObjects {[weak self] (feeds, error) in

            if let feeds = feeds {

                self?.deleteAllObjects()
                
                self?.context.perform({

                    _ = self?.objectConverter.convertToManagedObjects(from: feeds)
                    self?.saveContext()
                    self?.update()

                })

            }

        }
        
    }
    
    private func deleteAllObjects() {

        if let objectsToDelete = requestFeeds() {

            context.performAndWait {
                for object in objectsToDelete {
                    self.context.delete(object)
                }

            }

        }
        
    }
    
    //MARK: Core Data stack
    private lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "NewsReader")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    private var context:NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    
    private func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
                print("Saved!")
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}
