//
//  AppDelegate.swift
//  NewsReader
//
//  Created by Renat Gafarov on 04/05/2017.
//  Copyright © 2017 Renat Gafarov. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var configurator:AppConfigurator?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        configurator = AppConfigurator(window: window)
        configurator?.start()
        
        return true
    }

}
