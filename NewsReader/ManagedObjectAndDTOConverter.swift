//
//  ManagedObjectAndDTOConverter.swift
//  NewsReader
//
//  Created by Renat Gafarov on 04/05/2017.
//  Copyright © 2017 Renat Gafarov. All rights reserved.
//

import Foundation
import CoreData

class ManagedObjectAndDTOConverter {
    
    var context:NSManagedObjectContext
    
    init(with context:NSManagedObjectContext) {
        self.context = context
    }
    
    // Здесь нужен маппер, но ввиду небольшого количества времени, будет небольшой хардкод
    
    func convertToDTOs(from managedObjects:[Feed]) -> [FeedDTO]? {

        var result = [FeedDTO]()

        for managedObject in managedObjects {

            guard let stringURL = managedObject.url, let title = managedObject.title, let date = managedObject.date as Date? else {
                print("Broken object")
                continue
            }

            if let url = URL(string: stringURL) {
                let feedDTObject = FeedDTO(title: title, url: url, date: date)
                result.append(feedDTObject)
            }
        }

        return result.isEmpty ? nil : result
        
    }

    func convertToManagedObjects(from feedDTObjects:[FeedDTO]) -> [Feed]? {
        
        var result = [Feed]()
        
        for feedDTO in feedDTObjects  {

            let managedObject:Feed = NSEntityDescription.insertNewObject(forEntityName: "Feed", into: context) as! Feed
            managedObject.url = feedDTO.url.absoluteString
            managedObject.date = feedDTO.date as NSDate
            managedObject.title = feedDTO.title
            result.append(managedObject)

        }
        

        return result
    }
    
}
