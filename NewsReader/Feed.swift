//
//  Feed.swift
//  NewsReader
//
//  Created by Renat Gafarov on 04/05/2017.
//  Copyright © 2017 Renat Gafarov. All rights reserved.
//

import CoreData


class FeedDTO {
    
    var url:URL
    var title:String
    var date:Date
    
    init(title:String, url:URL, date:Date) {

        self.title = title
        self.url = url
        self.date = date

    }
    
}

@objc(Feed)
class Feed: NSManagedObject {
    
    @nonobjc public class func feedRequest() -> NSFetchRequest<Feed> {
        return NSFetchRequest<Feed>(entityName: "Feed")
    }
    
    @NSManaged public var url: String?
    @NSManaged public var date: NSDate?
    @NSManaged public var title: String?
    
}
