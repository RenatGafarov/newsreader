//
//  Storable.swift
//  NewsReader
//
//  Created by Renat Gafarov on 04/05/2017.
//  Copyright © 2017 Renat Gafarov. All rights reserved.
//

import Foundation

protocol Storable {

    weak var accessor: StorageAccessor? {get set}
    // Downloading from network
    func reloadData()
    // Fetching from CoreData
    func update()
}
