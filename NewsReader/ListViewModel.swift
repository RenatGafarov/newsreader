//
//  ListViewModel.swift
//  NewsReader
//
//  Created by Renat Gafarov on 04/05/2017.
//  Copyright © 2017 Renat Gafarov. All rights reserved.
//

import Foundation

class ListViewModel:StorageAccessor, ListViewModelProtocol {
    
    var storage:Storable
    
    
    var feeds:[FeedDTO]? {

        didSet{

            if feeds == nil {
                refresh()
                return
            }

            delegate?.didUpdatedViewModel()
            delegate?.endRefresh()

        }

    }
    
    var numberOfFeeds:Int {
        return feeds?.count ?? 0
    }
    
    weak var delegate:ListViewModelDelegate?
    
    init(storage:Storable) {
        self.storage = storage
    }
    
    @objc func refresh() {

        delegate?.startRefresh()
        storage.reloadData()
        
    }
    
    func update() {

        storage.update()

    }
    
    
    func viewModelForFeedWith(index:Int) -> FeedRepesentable? {
        
        if let feeds = feeds, index < feeds.count {

            let feedRepresent = FeedViewModel(feed: feeds[index])
            return feedRepresent

        } else {

            return nil

        }
        
    }


}
