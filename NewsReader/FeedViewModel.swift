//
//  FeedViewModel.swift
//  NewsReader
//
//  Created by Renat Gafarov on 04/05/2017.
//  Copyright © 2017 Renat Gafarov. All rights reserved.
//

import Foundation

protocol FeedRepesentable {

    var title:String {get}
    var date:String {get}
    var url:URL {get}

}

class FeedViewModel: FeedRepesentable {
    
    let date:String
    let title:String
    let url:URL
    
    init(feed:FeedDTO) {

        let formatter = DateFormatter()
        formatter.timeStyle = .short
        formatter.dateStyle = .medium
        self.date = formatter.string(from: feed.date)
        self.title = feed.title
        self.url = feed.url

    }
    
}
