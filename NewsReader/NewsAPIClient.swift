//
//  NewsAPIClient.swift
//  NewsReader
//
//  Created by Renat Gafarov on 04/05/2017.
//  Copyright © 2017 Renat Gafarov. All rights reserved.
//

import Foundation
import AlamofireRSSParser
import Alamofire

typealias RSSHandler = (_ data:[FeedDTO]?, _ error:Error?) -> Void

class NewsAPIClient:APIProtocol {
    
    //MARK:Public
    public func recieveObjects(completionHandler:@escaping RSSHandler) {
        downloadXML(completition: completionHandler)
    }
    
    //MARK:Privacy
    private let url = URL(string: "https://alfabank.ru/_/rss/_rss.html?subtype=1&category=2&city=21")!
    
    private func downloadXML(completition: @escaping RSSHandler) {
        
        Alamofire.request(url).responseRSS { (response) in
            if let channel = response.result.value {
                
                var result = [FeedDTO]()
                
                let feeds = channel.items
                for feed in feeds {
                    
                    guard let title = feed.title, let link = feed.link, let pubDate = feed.pubDate else {
                        print("Broken feed - \(feed)")
                        continue
                    }
                    
                    if let feedURL = URL(string: link) {
                        let feed = FeedDTO(title: title, url: feedURL, date: pubDate)
                        result.append(feed)
                    }
                }
                completition(result, nil)
            } else {
                completition(nil,response.error)
            }
        }
    }
}

