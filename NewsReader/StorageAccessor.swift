//
//  StorageAccessor.swift
//  NewsReader
//
//  Created by Renat Gafarov on 04/05/2017.
//  Copyright © 2017 Renat Gafarov. All rights reserved.
//

import Foundation

protocol StorageAccessor: class {

    var feeds:[FeedDTO]? {get set}

}
