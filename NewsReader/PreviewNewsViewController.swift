//
//  PreviewNewsViewController.swift
//  NewsReader
//
//  Created by Renat Gafarov on 04/05/2017.
//  Copyright © 2017 Renat Gafarov. All rights reserved.
//

import UIKit
import Social

class PreviewNewsViewController: UIViewController, UIWebViewDelegate {
    
    @IBOutlet weak var webView:UIWebView!
    @IBOutlet weak var activityIndicator:UIActivityIndicatorView!
    
    var viewModel:PreviewViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        loadWebView()
        addShareButton()
    }
    
    func addShareButton() {
        let rightButton = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(PreviewNewsViewController.share))
        navigationItem.rightBarButtonItem = rightButton
    }
    
    func share() {
        guard let url = self.viewModel?.url else {
            return
        }
        
        let actionSheet = UIAlertController(title: "Choose social", message: nil, preferredStyle: .actionSheet)
        let twitter = UIAlertAction(title: "Twitter", style: .default) { [unowned self] (action) in
            
            if let vc = SLComposeViewController(forServiceType: SLServiceTypeTwitter) {
                vc.setInitialText("Hey, friends. It's very important!")
                vc.add(url)
                self.present(vc, animated: true)
            }
            
        }
        
        let fb = UIAlertAction(title: "FaceBook", style: .default) { [unowned self] (action) in
            
            if let vc = SLComposeViewController(forServiceType: SLServiceTypeFacebook) {
                vc.setInitialText("Hey, friends. It's very important!")
                vc.add(url)
                self.present(vc, animated: true)
            }
            
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        actionSheet.addAction(twitter)
        actionSheet.addAction(fb)
        actionSheet.addAction(cancel)
        present(actionSheet, animated: true, completion: nil)
        
    }

    func loadWebView() {
        
        webView.delegate = self
        if let request = viewModel?.buildRequest() {
            webView.loadRequest(request)
        }
        
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        activityIndicator.startAnimating()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        activityIndicator.stopAnimating()
    }
    
    
}
