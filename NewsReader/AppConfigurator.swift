//
//  AppConfigurator.swift
//  NewsReader
//
//  Created by Renat Gafarov on 04/05/2017.
//  Copyright © 2017 Renat Gafarov. All rights reserved.
//

import UIKit


public class AppConfigurator {
    
    var window:UIWindow
    
    init(window:UIWindow?){
        guard let window = window else {
            fatalError("App can't start without window")
        }
        self.window = window
    }
    
    
    func start() {
        
        if let navigationController = window.rootViewController as? UINavigationController {

            if let listViewController = navigationController.viewControllers.first as? ListViewController {

                let storage = FeedProvider(api: NewsAPIClient())
                let viewModel = ListViewModel(storage: storage)
                storage.accessor = viewModel
                listViewController.viewModel = viewModel
                viewModel.delegate = listViewController
                configureUI()

            }

        } else {
            fatalError("Unknown rootViewController")
        }
        
    }
    
    func configureUI() {
        
        UINavigationBar.appearance().barTintColor = UIColor(red:0.93, green:0.19, blue:0.14, alpha:1.0)
        UINavigationBar.appearance().tintColor = UIColor.white

        if let barFont = UIFont(name: "Avenir-Light", size: 24.0) {

            UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white, NSFontAttributeName:barFont]

        }
        
        
    }
    
}

extension UINavigationController {

    open override var preferredStatusBarStyle: UIStatusBarStyle {

        return .lightContent

    }

}


